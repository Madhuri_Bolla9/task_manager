
import java.awt.BorderLayout;

import java.awt.Choice;

import java.awt.Color;

import java.awt.Dimension;

import java.awt.GridLayout;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.ResultSet;

import java.sql.Statement;




import javax.swing.Box;

import javax.swing.ButtonGroup;

import javax.swing.JButton;

import javax.swing.JDialog;

import javax.swing.JFrame;

import javax.swing.JLabel;

import javax.swing.JOptionPane;

import javax.swing.JPanel;

import javax.swing.JScrollPane;

import javax.swing.JTabbedPane;

import javax.swing.JTextArea;

import javax.swing.JTextField;




public class Frame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JLabel l1, l2, l3, l4, l5;

	JTextField t1, t3, t4;

	JTextArea t2;

	Choice jc;

	String db;

	JButton b1, b2, b3, bx;

	final JFrame theFrame = this;

	final JScrollPane listContainerPanel = new JScrollPane();

	final DatePicker picker = new DatePicker(this);

	@SuppressWarnings("unused")
	private static Frame o;

	@SuppressWarnings("unused")
	private ButtonGroup buttonGroup;

	@SuppressWarnings("unused")
	private String prio;

	@SuppressWarnings("unused")
	private String ddate;

	Frame() {

		super("TASK MANAGER");

		setSize(300, 400);




		JTabbedPane tb = new JTabbedPane();

		JPanel panelTab1 = new JPanel();

		panelTab1.setLayout(new BorderLayout());

		JPanel container = new JPanel();

		container.setLayout(new BorderLayout());

		JPanel panel = new JPanel();

		JPanel p = new JPanel();

		panel.setLayout(new GridLayout(3, 1));

		p.setLayout(new GridLayout(2, 2));

		l1 = new JLabel("Subject :");

		l1.setBounds(20, 50, 100, 30);

		p.add(l1);

		t1 = new JTextField();

		t1.setBounds(120, 50, 215, 30);

		p.add(t1);

		l2 = new JLabel("Description :");

		l2.setBounds(20, 90, 100, 30);

		p.add(l2);

		t2 = new JTextArea();

		t2.setBounds(120, 90, 215, 30);

		p.add(t2);




		panel.add(p);

		JPanel p2 = new JPanel();

		p2.setLayout(new GridLayout(1, 3));

		l3 = new JLabel("Due date :");




		l3.setBounds(20, 130, 100, 30);

		p2.add(l3);

		t3 = new JTextField();

		t3.setBounds(120, 130, 100, 30);




		bx = new JButton("...");




		p2.add(t3);

		p2.add(bx);




		bx.addActionListener(this);




		panel.add(p2);

		JPanel p3 = new JPanel();

		p3.setLayout(new GridLayout(3, 2));




		buttonGroup = new ButtonGroup();

		l4 = new JLabel("Alarm :");

		l4.setBounds(20, 170, 100, 30);

		p3.add(l4);

		t4 = new JTextField();

		t4.setBounds(120, 170, 215, 30);

		p3.add(t4);

		l5 = new JLabel("Priority :");

		l5.setBounds(20, 250, 100, 30);




		p3.add(l5);

		jc = new Choice();

		jc.add("high");

		jc.add("low");

		jc.setBounds(120, 250, 215, 30);

		p3.add(jc);




		b2 = new JButton("save");

		b2.addActionListener(this);

		b1 = new JButton("cancel");

		b2.addActionListener(this);

		p3.add(b2);

		p3.add(b1);

		b3 = new JButton("Refresh");

		b3.addActionListener(this);

		panel.add(p3);




		container.add(panel, BorderLayout.WEST);

		listContainerPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		listContainerPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		listContainerPanel.getVerticalScrollBar().setUnitIncrement(10);

		panelTab1.add(container);




		tb.addTab("Add task", panelTab1);

		JPanel panel2 = new JPanel();

		panel2.setLayout(new BorderLayout());

		panel2.add(b3, BorderLayout.NORTH);

		panel2.add(listContainerPanel, BorderLayout.CENTER);

		tb.addTab("View Task", panel2);




		add(tb);




		setVisible(true);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}




	public void actionPerformed(ActionEvent ae) {




		JButton b = (JButton) ae.getSource();




		if (b == b2) {

			ddate = t3.getText();

			String alarm = t4.getText();

			prio = jc.getSelectedItem().toString();

			try {




				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");




				Connection con = DriverManager

						.getConnection("jdbc:odbc:project");

				con.createStatement();

				if (alarm.equals(" "))

					JOptionPane.showMessageDialog(null,

							"Please Enter time in 24hr format");

				con.close();

			} catch (Exception e) {

				System.out.println(e.getMessage());

			}




		} else if (b == b1) {




			t1.setText("");

			t2.setText("");

			t3.setText("");

			t4.setText("");




		} else if (b == bx) {

			picker.setVisible(true);

			t3.setText(picker.setPickedDate());

		} else if (b == b3) {

			try {




				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

				Connection con = DriverManager

						.getConnection("jdbc:odbc:project");

				Statement st = con.createStatement();

				ResultSet rs = st.executeQuery("select * from task");

				Box vListBox = Box.createVerticalBox();

				listContainerPanel.getViewport().add(vListBox, null);

				while (rs.next()) {

					JPanel listItem = new JPanel();

					listItem.setLayout(new GridLayout(6, 2));

					listItem.add(new JLabel(""));

					listItem.add(new JLabel(""));

					listItem.add(new JLabel("Task No"));

					listItem.add(new JLabel(rs.getString(1)));

					listItem.add(new JLabel("Subject"));

					listItem.add(new JLabel(rs.getString(2)));

					listItem.add(new JLabel("Due date"));

					listItem.add(new JLabel(rs.getString(3)));

					listItem.add(new JLabel("Alarm time"));

					listItem.add(new JLabel(rs.getString(4)));

					listItem.add(new JLabel("Priority"));

					listItem.add(new JLabel(rs.getString(5)));

					vListBox.add(listItem);					

					

					//System.out.println(".......................");

					//System.out.println("Task NO:" + rs.getString(1));

					//System.out.println("subject:" + rs.getString(2));




					//System.out.println("due date:" + rs.getString(3));

					//System.out.println("alarm time:" + rs.getString(4));

					//System.out.println("priority:" + rs.getString(5));

				}

				vListBox.revalidate();

				

				con.close();

			} catch (Exception e) {

				System.out.println(e.getMessage());

			}

		}

	}




	class DatePicker {

		int month = java.util.Calendar.getInstance().get(

				java.util.Calendar.MONTH);

		int year = java.util.Calendar.getInstance()

				.get(java.util.Calendar.YEAR);;

		JLabel l = new JLabel("", JLabel.CENTER);

		String day = "";

		JDialog d;

		JButton b, b1, b2;

		JLabel l1, l2, l3, l4, l5;

		JTextField t1, t2, t3;

		JTextArea ta;

		JButton[] button = new JButton[49];




		public DatePicker(JFrame parent) {

			d = new JDialog();

			d.setModal(true);

			String[] header = { "Sun", "Mon", "Tue", "Wed", "Thur", "Fri",

					"Sat" };

			JPanel p1 = new JPanel(new GridLayout(7, 7));

			p1.setPreferredSize(new Dimension(430, 120));

			for (int x = 0; x < button.length; x++) {

				final int selection = x;

				button[x] = new JButton();

				button[x].setFocusPainted(false);

				button[x].setBackground(Color.white);

				if (x > 6)

					button[x].addActionListener(new ActionListener() {

						public void actionPerformed(ActionEvent ae) {

							day = button[selection].getActionCommand();

							d.dispose();

						}

					});

				if (x < 7) {

					button[x].setText(header[x]);

					button[x].setForeground(Color.red);

				}

				p1.add(button[x]);

			}

			JPanel p2 = new JPanel(new GridLayout(1, 3));

			JButton previous = new JButton("<< Previous");

			previous.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent ae) {

					month--;

					displayDate();

				}

			});

			p2.add(previous);

			p2.add(l);

			JPanel p = new JPanel();

			l1 = new JLabel("Task name");

			l2 = new JLabel("Description");

			l3 = new JLabel("Priority");

			l4 = new JLabel("Due date:");

			b2 = new JButton("date");

			l5 = new JLabel("Set alarm: time,date");

			t1 = new JTextField(20);

			t2 = new JTextField(5);

			t3 = new JTextField(5);

			ta = new JTextArea();

			ta.setEditable(false);

			Choice jc = new Choice();

			jc.add("high");

			jc.add("normal");

			jc.add("low");

			JButton b = new JButton("save");

			b.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent ae) {

				}

			});

			JButton b1 = new JButton("cancel");

			b1.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent ae) {

				}

			});

			p.setLayout(new GridLayout(6, 2));

			p.add(l1);

			p.add(t1);

			p.add(l2);

			p.add(ta);

			p.add(l3);

			p.add(jc);

			p.add(l4);

			p.add(t3);

			p.add(l5);

			p.add(t2);

			p.add(b);

			p.add(b1);

			JButton next = new JButton("Next >>");

			next.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent ae) {

					month++;

					displayDate();

				}

			});

			p2.add(next);

			d.add(p1, BorderLayout.CENTER);

			d.add(p2, BorderLayout.SOUTH);

			d.pack();

			d.setLocationRelativeTo(parent);

			displayDate();

		}

		

		public void setVisible(boolean visiblity){

			d.setVisible(visiblity);

		}




		public void displayDate() {

			for (int x = 7; x < button.length; x++)

				button[x].setText("");

			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(

					"MMMM yyyy");

			java.util.Calendar cal = java.util.Calendar.getInstance();

			cal.set(year, month, 1);

			int dayOfWeek = cal.get(java.util.Calendar.DAY_OF_WEEK);

			int daysInMonth = cal

					.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);

			for (int x = 6 + dayOfWeek, day = 1; day <= daysInMonth; x++, day++)

				button[x].setText("" + day);

			l.setText(sdf.format(cal.getTime()));

			d.setTitle("Date Picker");

		}




		public String setPickedDate() {

			if (day.equals(""))

				return day;

			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(

					"dd-MM-yyyy");

			java.util.Calendar cal = java.util.Calendar.getInstance();

			cal.set(year, month, Integer.parseInt(day));

			return sdf.format(cal.getTime());

		}

	}




	public static void main(String args[]) throws Exception {

		o = new Frame();




	}
}
